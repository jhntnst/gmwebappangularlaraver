<?php

namespace Database\Seeders;

use app\computadora;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class computadoraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('computadoras')->insert([
            'numequipo' => Str::random(5),
            'usuario' => Str::random(5),
            'tipo' => Str::random(5),
            'marca' => Str::random(5),
            'modelo' => Str::random(5),
            'ram' => Str::random(5),
            'discoduro' => Str::random(5),
            'tamañodd' => Str::random(5),
            'espaciout' => Str::random(5),
            'espaciodis' => Str::random(5),
            'so' => Str::random(5),
            'office' => Str::random(5),
            'antiviru' => Str::random(5),
            'empleado_id' => null,

        ]);
    }
}
