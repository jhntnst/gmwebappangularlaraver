<?php

namespace Database\Seeders;

use app\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        //esta linea es para insertar un registro a la ves
        /*DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'rol'=>'admin',
        ]);*/

        //esta linea hace que se siembren registros en la base de datos
       for($i = 0; $i < 20; $i++ ){
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'rol'=>'admin',

        ]);
        }
    }
}
