<?php

namespace Database\Seeders;

use app\Empleado;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class EmpleadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // for($i = 0; $i < 6; $i++){
        DB::table('empleados')->insert([
            'nombre' => Str::random(5),
            'appaterno' => Str::random(5).'p',
            'apmaterno' => Str::random(5).'m',
            'appaterno' => Str::random(5),
            'apmaterno' => Str::random(5),
            'puesto' => Str::random(5),
            'fechaingreso' => '2020/04/10',
            'diasvacaciones' => 1,
            'diasdescanso' => 2,
            'diastomados' => 3,
            'diasrestantes' => 4,
            'periodo' => Str::random(10),
            'observaciones' => Str::random(10),
            'user_id' => null,

        ]);
    //}
    }
}
