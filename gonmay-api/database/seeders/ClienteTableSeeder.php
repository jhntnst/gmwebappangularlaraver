<?php

namespace Database\Seeders;

use app\Cliente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('clientes')->insert([
            'razonsocial' => Str::random(5),
            'grupo' => Str::random(5).'p',
            'supervisorasig' => Str::random(5).'m',
            'estatusserv' => Str::random(5),
            'fechaentrega' => '2020-02-09 01:03:04',
            'servicio' => Str::random(5),
            'empleado_id' => null,

        ]);
    }
}
