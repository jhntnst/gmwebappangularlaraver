<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();

            $table->string('razonsocial');
            $table->string('grupo')->nullable();
            $table->string('supervisorasig');
            $table->string('estatusserv');
            $table->date('fechaentrega');
            $table->string('servicio');
            $table->unsignedBigInteger('empleado_id')->nullable();

            $table->foreign('empleado_id')->references('id')
            ->on('empleados')
            ->onDelete('set null');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
