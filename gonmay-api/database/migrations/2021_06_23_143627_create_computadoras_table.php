<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComputadorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computadoras', function (Blueprint $table) {
            $table->id();

            $table->string('numequipo', 10);
            $table->string('usuario');
            $table->string('tipo', 20);
            $table->string('marca', 30);
            $table->string('modelo', 30);
            $table->string('ram', 20);
            $table->string('discoduro', 20);
            $table->string('tamañodd', 20);
            $table->string('espaciout', 20);
            $table->string('espaciodis', 20);
            $table->string('so', 40);
            $table->string('office', 50);
            $table->string('antiviru', 50);

            $table->unsignedBigInteger('empleado_id')->nullable();

            $table->foreign('empleado_id')->references('id')->on('empleados')
            ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computadoras');
    }
}
