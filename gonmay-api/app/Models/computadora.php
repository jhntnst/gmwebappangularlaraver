<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class computadora extends Model
{
    use HasFactory;

    protected $fillable = [
        'numequipo',
        'usuario',
        'tipo',
        'marca',
        'modelo',
        'ram',
        'discoduro',
        'tamañodd',
        'espaciout',
        'espaciodis',
        'so',
        'office',
        'antiviru',
        'empleado_id',
    ];
}
