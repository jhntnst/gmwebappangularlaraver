<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'appaterno',
        'apmaterno',
        'puesto',
        'fechaingreso',
        'diasvacaciones',
        'diasdescanso',
        'diastomados',
        'diasrestantes',
        'periodo',
        'observaciones',
        'user_id',
    ];

}
